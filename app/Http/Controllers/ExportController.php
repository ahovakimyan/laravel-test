<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SoapBox\Formatter\Formatter;
use App\Http\Requests;
use Illuminate\Support\Facades\Storage;

class ExportController extends Controller
{

    public function add(Request $request){
        $id = md5(microtime(true));
        if($request->type == 'json'){
            $data = json_decode(Storage::get('/export/json.json'), true);
            $data[$id] = $request->except('type');
            $data[$id]['total'] = $request->quantity * $request->price;
            Storage::put('/export/json.json', json_encode($data));
        }elseif($request->type == 'xml'){
            $data  = Formatter::make(simplexml_load_string(Storage::get('/export/xml.xml')), Formatter::XML)->toArray();
            $xid = 'xml_'.$id;
            $data[$xid] = $request->except('type');
            $data[$xid]['total'] = $request->quantity * $request->price;
            $xml = Formatter::make($data, Formatter::ARR);
            Storage::put('/export/xml.xml', $xml->toXml());
        }
        return response()->json($id, 200);
    }

    public function edit(Request $request){
        if($request->attribute == 'json'){
            $data = json_decode(Storage::get('/export/json.json'), true);
            $data[$request->id][$request->object] = $request->value;
            if($request->object == 'quantity'){
                $data[$request->id]['total'] = $request->value * $data[$request->id]['price'];
            }elseif($request->object == 'price'){
                $data[$request->id]['total'] = $request->value * $data[$request->id]['quantity'];
            }
            Storage::put('/export/json.json', json_encode($data));
        }elseif($request->attribute == 'xml'){
            $data  = Formatter::make(simplexml_load_string(Storage::get('/export/xml.xml')), Formatter::XML)->toArray();
            $data[$request->id][$request->object] = $request->value;
            if($request->object == 'quantity'){
                $data[$request->id]['total'] = $request->value * $data[$request->id]['price'];
            }elseif($request->object == 'price'){
                $data[$request->id]['total'] = $request->value * $data[$request->id]['quantity'];
            }
            $xml = Formatter::make($data, Formatter::ARR);
            Storage::put('/export/xml.xml', $xml->toXml());
        }
        return response($request->value, 200);
    }

    public function index(){
        $json_data = json_decode(Storage::get('/export/json.json'), true);
        $xml_data = Formatter::make(simplexml_load_string(Storage::get('/export/xml.xml')), Formatter::XML)->toArray();
        if(isset($xml_data['item'])){
            unset($xml_data['item']);
        }
        return view('index.index', [
            'json' => $json_data,
            'xml' => $xml_data
        ]);
    }

    public function remove(Request $request){
        if($request->type == 'json'){
            $data = json_decode(Storage::get('/export/json.json'), true);
            unset($data[$request->id]);
            Storage::put('/export/json.json', json_encode($data));
        }elseif($request->type == 'xml'){
            $data  = Formatter::make(simplexml_load_string(Storage::get('/export/xml.xml')), Formatter::XML)->toArray();
            unset($data[$request->id]);
            $xml = Formatter::make($data, Formatter::ARR);
            Storage::put('/export/xml.xml', $xml->toXml());
        }
        return response('deleted', 200);
    }
}
