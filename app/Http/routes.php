<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'ExportController@index');

Route::any('export/add', 'ExportController@add');
Route::any('export/edit', 'ExportController@edit');
Route::any('export/remove', 'ExportController@remove');