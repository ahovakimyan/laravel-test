<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet" media="screen">
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" media="screen">

</head>
<body>
<div class="container">
    <div class="col-md-5">
        <div class="form-area">
            <form role="form" id="product_form" class="funkyradio">
                <br style="clear:both">
                <h3 style="margin-bottom: 25px; text-align: center;">Products</h3>
                <div class="form-group">
                    <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Product name" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Quantity in stock" required>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="price" name="price" placeholder="Price per item" required>
                </div>
                <div class="funkyradio-default">
                    <input type="radio" name="type" id="xml" value="xml" />
                    <label for="xml">XML</label>
                </div>
                <div class="funkyradio-primary">
                    <input type="radio" name="type" id="json" value="json" checked/>
                    <label for="json">JSON</label>
                </div>
                <button type="button" id="submit" name="submit" class="btn btn-primary pull-right">Add</button>
            </form>
        </div>
    </div>
    <div class="row col-md-6 col-md-offset-1 custyle">
        <br style="clear:both">
        <h3 style="margin-bottom: 25px; text-align: center;">From Json</h3>
        <table class="table table-striped custab">
            <thead>
            <tr>
                <th>Product name </th>
                <th>Quantity in stock</th>
                <th>Price per item</th>
                <th>Total</th>
                <th class="text-center">Action</th>
            </tr>
            </thead>
            <tbody id="json_table">
            @if(!empty($json))
                @foreach($json as $key => $val)
                <tr>
                    <td> <span class="editable" id="{{ $key }}" data-attribute="json" data-object="product_name" data-url="{{ url('export/edit') }}">{{ $val['product_name'] }}</span></td>
                    <td> <span class="editable" id="{{ $key }}" data-attribute="json" data-object="quantity" data-url="{{ url('export/edit') }}">{{ $val['quantity'] }}</span></td>
                    <td> <span class="editable" id="{{ $key }}" data-attribute="json" data-object="price" data-url="{{ url('export/edit') }}">{{ $val['price'] }}</span></td>
                    <td> <span data-object="total">{{ $val['total'] }}</span></td>
                    <td class="text-center"><button data-type="json" id="{{ $key }}" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Delete</button></td>
                </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        Total: <b id="json-total">0</b>
        <br style="clear:both">
        <h3 style="margin-bottom: 25px; text-align: center;">From XML</h3>
        <table class="table table-striped custab">
            <thead>
            <tr>
                <th>Product name </th>
                <th>Quantity in stock</th>
                <th>Price per item</th>
                <th>Total</th>
                <th class="text-center">Action</th>
            </tr>
            </thead>
            <tbody id="xml_table">
            @if(!empty($xml) && !isset($xml[0]))
                @foreach($xml as $key => $val)
                    <tr>
                        <td> <span class="editable" data-attribute="xml" id="{{ $key }}" data-object="product_name" data-url="{{ url('export/edit') }}">{{ $val['product_name'] }}</span></td>
                        <td> <span class="editable" data-attribute="xml" id="{{ $key }}" data-object="quantity" data-url="{{ url('export/edit') }}">{{ $val['quantity'] }}</span></td>
                        <td> <span class="editable" data-attribute="xml" id="{{ $key }}" data-object="price" data-url="{{ url('export/edit') }}">{{ $val['price'] }}</span></td>
                        <td> <span data-object="total">{{ $val['total'] }}</span></td>
                        <td class="text-center"><button data-type="xml" id="{{ $key }}" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Delete</button></td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        Total: <b id="xml-total">0</b>

    </div>
</div>

{{--scripts--}}
<script src="{{ asset('assets/js/jquery.v2.0.3.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery-validate/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/js/jinplace-1.2.1.min.js') }}"></script>

<script>
    var $product_form;
    var base_url = '<?php url()?>';
    $(document).ready(function(){
        var rules = {
            // Rules for form validation
            rules: {
                product_name: {
                    required: true,
                },
                quantity: {
                    required: true,
                    digits: true
                },
                price: {
                    required: true,
                    digits: true
                }
            },
            // Messages for form validation
            messages: {
                product_name: {
                    required: 'Please enter product name'
                },
                quantity: {
                    required: 'Please enter quantity',
                    digits: 'Please enter number'
                },
                price: {
                    required: 'Please enter price',
                    digits: 'Please enter number'

                }
            },

            // Do not change code below
            errorPlacement: function (error, element) {
                error.insertAfter(element);
            }
        };
        $product_form = $("#product_form");
        $product_form.validate(rules);

        $('.editable').jinplace();

        $('#submit').on('click', function(){
            if($product_form.validate(rules).errorList.length == 0){
                $.ajax({
                    url: base_url + 'export/add',
                    method: 'post',
                    data: $product_form.serializeArray(),
                    success: function(data){
                        if($('#json').is(':checked')){
                            $('#json_table').append(
                                    ' <tr>'+
                                        '<td> <span class="editable" data-attribute="json" id="'+ data +'" data-object="product_name" data-url="'+ base_url + 'export/edit' +'">'+ $('input[name=product_name]').val() +'</span></td>'+
                                        '<td> <span class="editable" data-attribute="json" id="'+ data +'" data-object="quantity" data-url="'+ base_url + 'export/edit' +'">'+ $('input[name=quantity]').val() +'</span></td>'+
                                        '<td> <span class="editable" data-attribute="json" id="'+ data +'" data-object="price" data-url="'+ base_url + 'export/edit' +'">'+ $('input[name=price]').val() +'</span></td>'+
                                        '<td> <span data-object="total" >'+ parseInt($('input[name=quantity]').val()) * parseInt($('input[name=price]').val()) +'</span></td>'+
                                        '<td class="text-center"><button data-type="json" id="'+ data +'" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Delete</button></td>'+
                                    '</tr>'
                            );
                        }else{
                            $('#xml_table').append(
                                    ' <tr>'+
                                        '<td> <span class="editable" data-attribute="xml" id="xml_'+ data +'" data-object="product_name" data-url="'+ base_url + 'export/edit' +'">'+ $('input[name=product_name]').val() +'</span></td>'+
                                        '<td> <span class="editable" data-attribute="xml" id="xml_'+ data +'" data-object="quantity" data-url="'+ base_url + 'export/edit' +'">'+ $('input[name=quantity]').val() +'</span></td>'+
                                        '<td> <span class="editable" data-attribute="xml" id="xml_'+ data +'" data-object="price" data-url="'+ base_url + 'export/edit' +'">'+ $('input[name=price]').val() +'</span></td>'+
                                        '<td> <span data-object="total" >'+ parseInt($('input[name=quantity]').val()) * parseInt($('input[name=price]').val()) +'</span></td>'+
                                        '<td class="text-center"><button data-type="xml" id="xml_'+ data +'" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Delete</button></td>'+
                                    '</tr>'
                            );
                        }
                        $('.editable').jinplace();
                        var xml_total = 0;
                        $('#xml_table tr').each(function(){
                            xml_total += parseInt($(this).find('span[data-object=total]').text());
                        });
                        $('#xml-total').text(xml_total);

                        var json_total = 0;
                        $('#json_table tr').each(function(){
                            json_total += parseInt($(this).find('span[data-object=total]').text());
                        });
                        $('#json-total').text(json_total);
                    },
                    error: function (request, status, error) {
                     //   if(request.status == 400)

                    }
                })
            }
        });

        $(document).on('click', '.btn-danger', function(){
            $this = $(this);
            var id = $(this).attr('id');
            var type = $(this).data('type');
            $.ajax({
                url: base_url + 'export/remove',
                method: 'post',
                data: {
                    id : id,
                    type : type
                },
                success: function(data){
                    $this.parents('tr').remove();
                    var xml_total = 0;
                    $('#xml_table tr').each(function(){
                        xml_total += parseInt($(this).find('span[data-object=total]').text());
                    });
                    $('#xml-total').text(xml_total);

                    var json_total = 0;
                    $('#json_table tr').each(function(){
                        json_total += parseInt($(this).find('span[data-object=total]').text());
                    });
                    $('#json-total').text(json_total);
                },
                error: function (request, status, error) {
                    //   if(request.status == 400)

                }
            })
        });

        $(document).on('blur', '.editable', function(){
            if($(this).data('object') == 'quantity'){
                var quant = $(this).find('input').val();
                var price = $(this).parents('tr').find('span[data-object=price]').text();
                var total = parseInt(quant) * parseInt(price);
                $(this).parents('tr').find('span[data-object=total]').text(total);
            }else if($(this).data('object') == 'price'){
                var price = $(this).find('input').val();
                var quant = $(this).parents('tr').find('span[data-object=quantity]').text();
                var total = parseInt(quant) * parseInt(price);
                $(this).parents('tr').find('span[data-object=total]').text(total);
            }
            var xml_total = 0;
            $('#xml_table tr').each(function(){
                xml_total += parseInt($(this).find('span[data-object=total]').text());
            });
            $('#xml-total').text(xml_total);

            var json_total = 0;
            $('#json_table tr').each(function(){
                json_total += parseInt($(this).find('span[data-object=total]').text());
            });
            $('#json-total').text(json_total);
        });

        var xml_total = 0;
        $('#xml_table tr').each(function(){
             xml_total += parseInt($(this).find('span[data-object=total]').text());
        });
        $('#xml-total').text(xml_total);

        var json_total = 0;
        $('#json_table tr').each(function(){
            json_total += parseInt($(this).find('span[data-object=total]').text());
        });
        $('#json-total').text(json_total);
    });
</script>

</body>
</html>